<?php


namespace App\Models;


use splx\Model;

/**
 * Class Admin
 * @package App\Models
 * @property string $login
 * @property string $pass_hash
 */
class Admin extends Model
{
    public const TABLE = 'admin';

}
