<?php


namespace App\Models;


use splx\Model;

class User extends Model implements HasEmail
{
    public const TABLE = 'users';

    public string $email;
    public string $name;

    protected static string $table = 'users';


    /**
     * @return string
     */
    public function getEmail():string
    {
        return $this->email;
    }
}
