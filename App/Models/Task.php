<?php


namespace App\Models;


use splx\Model;

/**
 * Class Task
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $text
 * @property string $status
 * @property bool edited_by_admin
 */
class Task extends Model implements HasEmail
{
    public const TABLE = 'task';

    public function getEmail(): string
    {
        return $this->email;
    }

    public function save()
    {


        $this->id = $this->insertTask();

        return $this;
    }


}

