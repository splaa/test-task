<?php


namespace App\Models;


interface HasEmail
{
    /**
     * Method returning an email address
     * @return string E-mail address
     */
    public function getEmail(): string;

}
