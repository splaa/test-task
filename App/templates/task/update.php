<?php
/**
 * @var  $tasks
 * @var Task $task
 * @var string $title
 */


use App\Models\Task;

?>

<main role="main" class="flex-shrink-0" style="margin: 40px;">
    <div class="container">
        <h1 class="mt-5">Create New Task</h1>

        <div class="panel panel-default">
            <div class="panel-heading">Task Name: <?php echo $task->name ?? '-= Без Имени =-' ?></div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="/task/create/">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text"
                                   class="form-control"
                                   id="inputName"
                                   name="name"
                                   placeholder="<?php echo $task->name ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmai" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email"
                                   class="form-control"
                                   id="inputEmail"
                                   name="email"
                                   placeholder="<?php echo $task->email ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Text Task</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="text"
                                      placeholder="<?php echo $task->text ?>"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
</main>
