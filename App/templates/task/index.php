<?php
/**
 * @var  Pagination $tasks
 * @var Task $task
 * @var string $title
 */

use App\Models\Task;
use App\Services\Pagination;

?>

<main style="margin-top: 50px" role="main" class="flex-shrink-0">
    <div class="container">
        <h1 class="mt-5">Задачи</h1>

        <div class="btn-group" role="group" aria-label="Basic example">

            <a href="/task/?page=<?= $page ?>&sort=<?= $sortType ?>&order=up" class="btn btn-secondary sort"> Up </a>
            <a href="/task/?page=<?= $page ?>&sort=<?= $sortType ?>&order=down" class="btn btn-secondary sort">
                Down </a>
            <a href="/task/?page=<?= $page ?>&sort=name&order=<?= $order ?>" class="btn btn-secondary sort"> Name </a>
            <a href="/task/?page=<?= $page ?>&sort=email&order=<?= $order ?>" class="btn btn-secondary sort">E-mail</a>
            <a href="/task/?page=<?= $page ?>&sort=status&order=<?= $order ?>" class="btn btn-secondary sort">Status</a>
        </div>
        <div class="row">
            <?php foreach ($tasks->currentPageResults as $task): ?>
                <div class="card border-success mb-3 col-sm-3" style=" margin: 7px;">
                    <div class="card-header  border-success">
                        <?php $color = ('done' === $task->status) ? 'class="bg-success text-white"' : 'class="bg-danger text-white"' ?>
                        Status:<br> task-<?= $task->id ?>:
                        <span <?= $color ?> ><?php echo $task->status ?? '-= Без Status =-' ?></span>
                        <p class="bg-warning text-info"><?php echo $task->edited_by_admin ? 'отредактировано администратором' : '' ?></p>
                    </div>
                    <a href="/task/one/?id=<?= $task->id ?>">
                        <div class="card-body">
                            <h5><?php echo $task->text ?? '-= Без text =-' ?></h5>

                        </div>
                    </a>
                    <div class="card-footer bg-transparent border-success">
                        Имя : <?php echo $task->name ?? '-= Без Имени =-' ?>
                        <p>email: <a
                                    href="mailto:<?php echo $task->email ?? '-= Без Email =-' ?>"><?php echo $task->email ?? '-= Без Email =-' ?></a>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?></div>
    </div>
</main>
<div class="pagination">
    <?php echo $tasks->getPaginationOut() ?>
</div>

