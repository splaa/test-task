<?php
/**
 * @var  $tasks
 * @var Task $task
 * @var string $title
 */


use App\Models\Task;
?>


<main role="main" class="flex-shrink-0" style="margin: 40px;">
    <div class="container">
        <h1 class="mt-5">Create New Task</h1>
        <!--/todo-splaa: upgrade to React-->
        <div class="panel panel-default">
            <div class="panel-heading">Task Name: New Task</div>
            <div class="panel-body">
                <form id="form" class="form-horizontal" method="post" action="/task/create/">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text"
                                   class="form-control"
                                   id="inputName"
                                   name="name"
                                   placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmai" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email"
                                   class="form-control"
                                   id="inputEmail"
                                   name="email"
                                   placeholder="Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Text Task</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="text""></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
</main>
