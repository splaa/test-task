<?php
/**
 * @var  $tasks
 * @var Task $task
 * @var string $title
 */


use App\Models\Task;

?>
<h2>Admin Panel</h2>
<a href="/task/index" class="btn btn-primary"><--Back Home</a>
<a href="/task/create" class="btn btn-success">New Task</a>
<a href="/task/one/?id=<?php echo $task->id ?? '' ?>" class="btn btn-info">View</a>

<main role="main" class="flex-shrink-0" style="margin: 40px;">
    <div class="container">
        <h1 class="mt-5">Update Task Id: <?php echo $task->id ?></h1>

        <div class="panel panel-default">
            <div class="panel-heading">Task
                Name: <?php echo $task->id . ' : ' . $task->name ?? '-= Без Имени =-' ?></div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="/admin/update/?id=<?= $task->id ?>">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text"
                                   class="form-control"
                                   id="inputName"
                                   name="name"
                                   placeholder="<?php echo $task->name ?>"
                                   value="<?php echo $task->name ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmai" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email"
                                   class="form-control"
                                   id="inputEmail"
                                   name="email"
                                   placeholder="<?php echo $task->email ?>"
                                   value="<?php echo $task->email ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Text Task</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="text"">
                            <?php echo html_entity_decode($task->text, ENT_QUOTES, 'UTF-8'); ?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php $checked = 'done' === $task->status ? 'checked' : '' ?>
                        <label class="col-sm-2 control-label">Done/Not Done</label>
                        <div class="checkbox col-sm-4">
                            <input name="status" <?= $checked ?> type="checkbox" style="margin-left: 2px;">

                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
</main>
