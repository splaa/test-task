<h2>Admin Panel</h2>
<a href="/task/" class="btn btn-primary"><--Back</a>
<a href="/task/create" class="btn btn-success">New Task</a>


<main role="main" class="flex-shrink-0" style="margin: 40px;">
    <div class="container">
        <h1 class="mt-5">Log in</h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal" id="form-login" method="post" action="/admin/login/?id=<?= $id ?>">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Login</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="login" placeholder="Login">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Sign in</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
</main>
