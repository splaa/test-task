<?php

/**
 * @var Task $task
 */


use App\Models\Task;

?>
<a href="/task/" class="btn btn-primary"><--Back</a>
<a href="/task/create" class="btn btn-success">New Task</a>
<a href="/admin/login/?id=<?php echo $task->id ?>" class="btn btn-info">Edit</a>

<main role="main" class="flex-shrink-0" style="margin: 40px;">
    <div class="container">
        <h1 class="mt-5">Task Id: <?php echo $task->id ?></h1>
        <div class="panel panel-default">
            <div class="panel-heading"><?php echo $task->name ?? '-= Без Имени =-' ?></div>
            <div class="panel-body"><?php echo $task->email ?? '-= Без Email =-' ?></div>
            <div class="panel-body"><?php echo $task->text ?? '-= Без Email =-' ?></div>
            <div class="panel-body"><?php echo $task->status ?? '-= Без Email =-' ?></div>
        </div>

    </div>
</main>
