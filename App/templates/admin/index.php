<?php
/**
 * @var  Pagination $tasks
 * @var Task $task
 * @var string $title
 */


use App\Models\Task;
use App\Services\Pagination;


?>
<h2>Admin Panel</h2>

<a href="/admin/create" class="btn btn-success">New Task</a>

<main style="margin-top: 50px" role="main" class="flex-shrink-0">
    <div class="container">
        <h1 class="mt-5">Задачи</h1>

        <div class="btn-group" role="group" aria-label="Basic example">
            <a href="http://php2.loc/task/?page=<?= $page ?>&sort=name" class="btn btn-secondary sort">Name</a>
            <a href="http://php2.loc/task/?page=<?= $page ?>&sort=email" class="btn btn-secondary sort">E-mail</a>
            <a href="http://php2.loc/task/?page=<?= $page ?>&sort=status" class="btn btn-secondary sort">Status</a>
        </div>
        <div class="row">
            <?php foreach ($tasks->currentPageResults as $task): ?>

                <div class="card border-success mb-3 col-sm-3" style=" margin: 7px;">
                    <div class="card-header  border-success">
                        <?php $color = ('done' === $task->status) ? 'class="text-success"' : 'class="text-danger"' ?>
                        Status: <span <?= $color ?> ><?php echo $task->status ?? '-= Без Status =-' ?></span></div>
                    <a href="/admin/update/?id=<?= $task->id ?> ">
                        <div class="card-body">
                            <!--                            <h5>-->
                            <?php //echo html_entity_decode($task->text, ENT_QUOTES, 'UTF-8') ?? '-= Без text =-' ?><!--</h5>-->
                            <h5><?php echo $task->text ?? '-= Без text =-' ?></h5>

                        </div>
                    </a>
                    <div class="card-footer bg-transparent border-success">
                        name: <?php echo $task->name ?? '-= Без Имени =-' ?>
                        <p>email: <a
                                    href="mailto:<?php echo $task->email ?? '-= Без Email =-' ?>"><?php echo $task->email ?? '-= Без Email =-' ?></a>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?></div>
    </div>
</main>
<div class="pagination">
    <?php echo $tasks->getPaginationOut() ?>
</div>
