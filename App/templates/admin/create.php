<?php
/**
 * @var  $tasks
 * @var Task $task
 * @var string $title
 */


use App\Models\Task;

?>
<h2>Admin Panel</h2>
<?php include __DIR__ . '/nav_block.php' ?>

<main role="main" class="flex-shrink-0" style="margin: 40px;">
    <div class="container">
        <h1 class="mt-5">Create New Task</h1>
        <!--/todo-splaa: upgrade to React-->
        <div class="panel panel-default">
            <div class="panel-heading">Task Name: New Task</div>
            <div class="panel-body">
                <form class="form-horizontal" id="form" method="post" action="/admin/create/">
                    <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text"
                                   class="form-control"
                                   id="inputName"
                                   name="name"
                                   placeholder="Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmai" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email"
                                   class="form-control"
                                   id="inputEmail"
                                   name="email"
                                   placeholder="Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Text Task</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="text""></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
</main>

<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-muted">Place sticky footer content here.</span>
    </div>
</footer>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>


</body>
</html>
