<?php
/**
 * @var  Pagination $tasks
 * @var Task $task
 * @var string $title
 * @var $content
 */

$title = 'BeeJee Test';
$status = '';

use App\Models\Task;
use App\Services\Pagination;


?>

<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title><?= $title ?></title>
    <!-- Bootstrap3 core Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!--     Bootstrap4 core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="/css/sticky-footer.css">
    <!--    <link href="sticky-footer-navbar.css" rel="stylesheet">-->
</head>
<body class="d-flex flex-column h-100">
<header>
    <!-- Fixed navbar -->

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="https://beejee.ru/career">Тестовое от BeeJee </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/task">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/task/create">New Task<span class="sr-only">(current)</span></a>
                </li>

                <?php if (isset($_SESSION['user']) && 'admin' === $_SESSION['user']): ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="/admin/logout">LogOut <span class="sr-only">(current)</span></a>
                    </li>
                <?php else: ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="/admin/login">Login <span class="sr-only">(current)</span></a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
</header>

<!-- Begin page content -->
<div class="container" style="margin: 80px">
    <?php echo $content ?>

    <?php if (isset($_SESSION['success']) && !empty($_SESSION['success'])): ?>
        <?php
        $message = isset($_SESSION['success']) ? $_SESSION['success'] : null;
        unset($_SESSION['success']);
        ?>
        <div class="alert alert-success alert-dismissible" id="successAlert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <?php echo $message ?> .
            <a href="/admin/login" class="alert-link">Login</a>.
        </div>
    <?php endif; ?>
    <?php if (isset($_SESSION['error']) && !empty($_SESSION['error'])): ?>
        <?php
        $message = isset($_SESSION['error']) ? $_SESSION['error'] : null;

        ?>
        <div class="alert alert-danger alert-dismissible" id="errorAlert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <?php echo $message ?>
            <a href="/admin/login/" class="alert-link">Login link</a>. Give it a click if you like.
        </div>
        <?php unset($_SESSION['error']); endif; ?>

</div>

<!-- End page content -->
<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-muted">Карьера в BeeJee</span>
        <p>От джуна до тимлида за 2 года</p>
    </div>
</footer>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="/js/jquery.validate.js"></script>
<script>
    // валидация полей формы
    $(function () {
        $('#form').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3

                },
                email: {
                    required: true

                },
                text: {
                    required: true,
                    minlength: 7
                }

            },
            messages: {
                name: {
                    required: "Поле Имя обязательно к заполнению",
                    minlength: "Введите не менее 3-х символов в поле Имя"
                },
                email: {
                    required: "Поле 'Email' обязательно к заполнению",
                    email: "Необходим формат адреса email"
                },
                text: {
                    required: "Поле Text обязательно к заполнению",
                    minlength: "Введите не менее 10 символов  в поле Text"
                }
            }
        });
    });
    $(function () {
        $('#form-login').validate({
            rules: {
                login: {
                    required: true,
                    minlength: 3

                },
                password: {
                    required: true

                },
            },
            messages: {
                login: {
                    required: "Поле Logi обязательно к заполнению",
                    minlength: "Введите не менее 3-х символов в поле Login"
                },
                password: {
                    required: "Поле Password обязательно к заполнению",
                },
            }
        });
    });


</script>
<?php
if (isset($message)):?>

    <script>
        $(function () {
            window.setTimeout(function () {
                $('#successAlert').alert('close');
            }, 3000);
        });

        $(function () {
            window.setTimeout(function () {
                $('#errorAlert').alert('close');
            }, 3000);
        });
    </script>
<?php else: ?>
    <script>
        $(".alert").alert('close')
    </script>
<?php endif; ?>
</body>
</html>
