<?php


namespace App\Controllers;


use splx\Controller;
use splx\View;

class WebController extends Controller
{
    public $title;

    public function render($template, $message = [])
    {

        $pageView = new View();

        foreach ($message as $key => $value) {

            $pageView->$key = $value;

        }


        $pathTemplate = __DIR__ . '/../templates/' . $template . '.php';
        $this->view->content = $pageView->render($pathTemplate);


        $this->view->display(__DIR__ . '/../templates/layout/main.php');
    }
}
