<?php


namespace App\Controllers;


use App\Models\Task as TaskModel;
use App\Services\Pagination;


class Admin extends WebController
{
    public function actionLogin()
    {
        if (isset($_POST['login']) || isset($_POST['password'])) {

            $login = $_POST['login'] ?? '';
            $password = $_POST['password'] ?? '';
            $id = 1;

            if ('admin' === $login && '123' === $password) {
                unset($_SESSION['error']);
                $user = $_SESSION['user'] = 'admin';
                $_SESSION['success'] = 'Вы успешно вошли как admin';
                header("Location: /admin/update/?id=" . $id);
            }

            $_SESSION['error'] = 'Login as Admin';
        }
        $id = $_GET['id'] ?? '';
        $this->render('/admin/login', compact('id'));
    }

    public function actionLogout()
    {
        unset($_SESSION['user']);
        $_SESSION['success'] = 'Вы успешно вышли';
        header("Location: /task/");
    }

    public function actionUpdate()
    {


        /**
         * @var TaskModel $task
         */
        $task = TaskModel::findById($this->getId());

        $taskText = $task->text;
//var_dump(strcmp($taskText, 'uwgieib'));


        if (!empty($_POST)) {

            $task->name = $_POST['name'] ?? '-=no name=-';
            $task->email = $_POST['email'] ?? '-=no email=-';
            $task->text = htmlentities(trim($_POST['text']), ENT_QUOTES, "UTF-8") ?? '-=no text=-';
            $task->status = (isset($_POST['status']) && 'on' === $_POST['status'])
                ? 'done' : 'not done';
            if (isset($task->id)) {
                $task->edited_by_admin = (strcmp($taskText, $task->text) !== 0) ? true : false;
                $task->update();
            } else {
                $task->save();
                die('save');
            }
            $_SESSION['success'] = 'Задача обновлена успешно ';

        }
        $this->render('/admin/update', compact('task'));
    }

    private function getId()
    {
        return $_GET['id'] ?? 1;
    }

    protected function actionCreate()
    {
        $task = new TaskModel();

        if (!empty($_POST)) {

            $task->name = $_POST['name'] ?? '-=no name=-';
            $task->email = $_POST['email'] ?? '-=no email=-';
            $task->text = htmlentities(trim($_POST['text']), ENT_QUOTES, "UTF-8") ?? '-=no text=-';

            $task->save();
            $_SESSION['success'] = 'Задача добавлена успешно ';

            header('Location: /admin/update/?id=' . $task->id);
            exit;
        }

        $this->render('admin/create');
    }

    protected function beforeAction()
    {

        if (isset($_SESSION['user']) || (isset($_SESSION['user']) && 'admin' === $_SESSION['user'])) {


        } else {
            $this->actionLogin();
        }
    }

    protected function actionIndex(): void
    {

        $pagination = new Pagination();


        $pagination->render(TaskModel::findAllSort($this->getTyeSort(), $this->getSortUpDown()));

        $tasks = $pagination;
        $page = $_GET['page'] ?? 1;
        $this->render('/admin/index', compact('tasks', 'page'));
    }

    private function getTyeSort(): string
    {

        $listTypeSort = ['name', 'email', 'status'];

        $typeSort = $listTypeSort[0];

        if (isset($_GET['sort'])) {
            foreach ($listTypeSort as $typeSortItem) {
                if ($_GET['sort'] === $typeSortItem) {
                    $typeSort = $typeSortItem;
                }
            }
        }

        return $typeSort;
    }

    private function getSortUpDown()
    {
        $listSortUpDown = ['up', 'down'];
        $typeOrderBy = $listSortUpDown[0];
        if (isset($_GET['order'])) {
            foreach ($listSortUpDown as $value) {
                if ($_GET['order'] === $value) {
                    $typeOrderBy = $_GET['order'];
                }
            }
        }
        return $typeOrderBy;
    }


}

