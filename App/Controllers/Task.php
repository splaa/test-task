<?php


namespace App\Controllers;

use App\Models\Task as TaskModel;
use App\Services\Pagination;

class Task extends WebController
{


    protected function actionIndex(): void
    {
        $pagination = new Pagination();
        $sortType = $this->getTyeSort();

        $sortOrderBy = $this->getOrderUpDown();
        $order = $sortOrderBy ? 'up' : 'down';

        $pagination->render(TaskModel::findAllSort($sortType, $sortOrderBy));

        $tasks = $pagination;
        $page = $_GET['page'] ?? 1;

        $this->render('/task/index', compact('page', 'tasks', 'order', 'sortType'));
    }

    private function getTyeSort(): string
    {

        $listTypeSort = ['name', 'email', 'status'];

        $typeSort = $listTypeSort[0];

        if (isset($_GET['sort'])) {
            foreach ($listTypeSort as $typeSortItem) {
                if ($_GET['sort'] === $typeSortItem) {
                    $typeSort = $typeSortItem;
                }
            }
        }

        return $typeSort;
    }

    private function getOrderUpDown(): bool
    {
        $order = true;
        if (isset($_GET['order']) && 'up' === $_GET['order']) {
            $order = true;
        }
        if (isset($_GET['order']) && 'down' === $_GET['order']) {
            $order = false;
        }
        return $order;
    }

    protected function actionOne($idNew = 1): void
    {

        $id = $_GET['id'] ?? $idNew;


        $task = TaskModel::findById($id);

        $this->render('/task/one', compact('task'));
    }

    protected function actionCreate(): void
    {

        $task = new TaskModel();

        if (!empty($_POST)) {
            $task->name = $_POST['name'] ?? '-=no name=-';
            $task->email = $_POST['email'] ?? '-=no email=-';
            $task->text = htmlentities(trim($_POST['text']), ENT_QUOTES, "UTF-8") ?? '-=no text=-';

            $task->save();
            $_SESSION['success'] = 'Задача добавлена успешно ' . $task->id;

            header('Location: /task/one/?id=' . $task->id);
            exit();


        }

        $this->render('/task/create');

    }

    protected function actionUpdate(): void
    {
        if ($user = false) {


            $id = $_GET['id'] ?? 1;

            $task = TaskModel::findById($id);

            if (!empty($_POST)) {

                $task->name = $_POST['name'] ?? '-=no name=-';
                $task->email = $_POST['email'] ?? '-=no email=-';
                $task->text = htmlentities(trim($_POST['text']), ENT_QUOTES, "UTF-8") ?? '-=no text=-';
                $task->save();

            }
            $this->render('/task/update', compact('task'));

        } else {
            header('Location: /admin/login/');
        }
    }

    protected function getSortTask($typeSort, $orderBy)
    {

        return TaskModel::findAllSort($typeSort, $orderBy);
    }

}
