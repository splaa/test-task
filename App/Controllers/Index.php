<?php


namespace App\Controllers;


class Index extends WebController
{
    public function actionIndex(): void
    {
        $message = 'Splx';

        $this->render('/index/index', compact('message'));
    }

}
