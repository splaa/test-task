<?php


namespace App\Services;

use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\DefaultView;

class Pagination
{


    public $currentPageResults;
    /**
     * @var ArrayAdapter
     */
    private $adapter;
    /**
     * @var Pagerfanta
     */
    private $pagerfanta;
    private $currentPage;
    private $nbResults;
    private $routeGenerator;
    private $options;
    /**
     * @var string|string[]
     */
    private $pagination_out;

    public function render($tasks, $maxPerPage = 3)
    {
        $this->adapter = new ArrayAdapter($tasks);
        $this->pagerfanta = new Pagerfanta($this->adapter);

        $this->pagerfanta->setMaxPerPage($maxPerPage); // 10 by default
        $maxPerPage = $this->pagerfanta->getMaxPerPage();

        if (isset($_GET["page"])) {
            $this->pagerfanta->setCurrentPage($_GET["page"]);
        }
        $this->currentPage = $this->pagerfanta->getCurrentPage();

        $this->nbResults = $this->pagerfanta->getNbResults();
        $this->currentPageResults = $this->pagerfanta->getCurrentPageResults();

        $this->routeGenerator = function ($page) {
            $sort = '&sort=';
            $sort .= $_GET['sort'] ?? 'test';

            $order = '&order=';
            $order .= $_GET['order'] ?? 'up';

            return '/task/?page=' . $page . $sort . $order;
        };

        $view = new DefaultView();
        $this->options = array(
            'proximity' => 3,
            'css_disabled_class' => 'enable'
        );
        $this->pagination_out = $view->render($this->pagerfanta, $this->routeGenerator, $this->options);
    }

    /**
     * @return string|string[]
     */
    public function getPaginationOut()
    {
        return $this->pagination_out;
    }

}




