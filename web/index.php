<?php
session_start();

require __DIR__ . '/../autoload.php';

$uri = $_SERVER['REQUEST_URI'];

$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);



$parts = explode('/', $uri_parts[0]);

$controllerName = $parts[1] ? ucfirst($parts[1]) : 'Task';

$action = !empty($parts[2]) ? ucfirst($parts[2]) : 'Index';

$class = '\App\Controllers\\' . trim($controllerName);





$controller = new $class();
$controller->action($action);
