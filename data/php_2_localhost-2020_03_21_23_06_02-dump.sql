-- MySQL dump 10.13  Distrib 5.7.26, for osx10.10 (x86_64)
--
-- Host: 127.0.0.1    Database: php_2
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task`
(
    `id`              int(11)      NOT NULL AUTO_INCREMENT,
    `name`            varchar(64)  NOT NULL,
    `email`           varchar(255) NOT NULL,
    `text`            text,
    `status`          enum ('done','not done') DEFAULT 'not done',
    `edited_by_admin` tinyint(1)   NOT NULL    DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task`
    DISABLE KEYS */;
INSERT INTO `task`
VALUES (1, 'Генадий Лавринюк', 'splaandrey@gmail.com',
        'йцуйцуйцу      &lt;script&gt;alert(&lsquo;test&rsquo;);&lt;/script&gt;', 'done', 0),
       (2, 'Валерий Лавринюк', 'aandrey@gmail.com',
        'aseae      ewrtqertqert  sddfgsfdgsfdgscript&gt;alert(&lsquo;test&rsquo;);&lt;/script&gt;', 'done', 1),
       (3, 'Андрей Лавринюк', 'y@gmail.com',
        'sertwerwtert                        Задача:8 &lt;script&gt;alert(&lsquo;test&rsquo;);&lt;/script&gt;', 'done',
        1),
       (4, 'Юра', 'l@gmail.com', 'efe                     &lt;script&gt;alert(&lsquo;test&rsquo;);&lt;/script&gt;',
        'not done', 1),
       (5, 'Света Лавринюк', 'Vplaandrey@gmail.com', '&lt;script&gt;alert(&lsquo;test&rsquo;);&lt;/script&gt;',
        'not done', 0);
/*!40000 ALTER TABLE `task`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-25 11:01:28
