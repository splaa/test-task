<?php


namespace splx;


class Config
{
    use Singleton;

    public array $data = [];


    public static function instance()
    {
        if (null === static::$instance) {

            static::$instance = new static();
        }
        return static::$instance;
    }

}
