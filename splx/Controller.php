<?php


namespace splx;


use App\Controllers\Task;

abstract class Controller
{
    protected $view;
    public $layout = __DIR__ . '/../App/templates/layout/main.php';

    /**
     * Task constructor.
     */
    public function __construct()
    {
        $this->view = new View();

    }

    public function action(string $action)
    {
        $methodName = 'action' . $action;
        $this->beforeAction();
        return $this->$methodName();
    }


    protected function beforeAction()
    {
//        Access
//        throw new Core('Сообщение об исключении!! ' . self::class);
    }
}
