<?php


namespace splx;


class View implements \Countable
{
//    start trait   //todo-splaa: Refactor in trait

    protected $data = [];

    /**
     * View constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;

    }

    public function __get($name)
    {
        return $this->data[$name];
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }


    // -=end trait=-

    public function display(string $template): void
    {

        echo $this->render($template);
    }

    public function render($template)
    {

        ob_start();

        foreach ($this->data as $prop=>$value) {
            $$prop = $value;

        }

        include $template;

        $content = ob_get_contents();

        ob_end_clean();
        return $content;
    }


    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->data);
    }
}
