<?php


namespace splx;


class Db
{
    use Singleton;

    protected $dbh;
    private $id;

    /**
     * Db1 constructor.
     */
    protected function __construct()
    {
        try {
            if ('php2.loc' === $_SERVER["HTTP_HOST"]) {
                $host = '127.0.0.1';
            } else {
                $host = 'mysql';

            }


            $this->dbh = new \PDO('mysql:host=' . $host . ';dbname=php_2', 'root', 'root');
        } catch (\PDOException $e) {

            throw new \App\Exeptions\Db('Что то с базой');
        }
    }

    public function execute($sql, array $params = [])
    {
        $sth = $this->dbh->prepare($sql);


        return $sth->execute($params);
    }

    public function insert($sql, array $params = [])
    {
        $sth = $this->dbh->prepare($sql);
        $st = $sth->execute($params);


        return $this->getLastId();
    }

    public function query($sql,array $params = [],$class)
    {

        $sth = $this->dbh->prepare($sql);

        if ($sth->execute($params)) {


            $res = $sth->fetchAll(\PDO::FETCH_CLASS,
                $class);

            return $res;


        }
        return null;

    }

    public function getLastId()
    {
        return $this->dbh->lastInsertId();
    }
}
