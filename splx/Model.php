<?php


namespace splx;


abstract class Model
{
    public const TABLE = '';
    public $id;

    public static function findAll($data = [])
    {

        $db = Db::instance();
        $sql = 'select * from ' . static::TABLE;

        if (!empty($data['filter_name'])) {

        }

        if (!empty($data['filter_email'])) {

        }

        if (!empty($data['filter_status'])) {

        }

        return $db->query($sql, [], static::class);
    }

    public static function findAllSort(string $name, $order = true)
    {
        $sortBy = $order ? 'ASC' : 'DESC';
        $db = Db::instance();
        $sql = 'select * from ' . static::TABLE .
            ' order by ' . $name . ' ' . $sortBy;

        return $db->query($sql, [], static::class);
    }

    public static function findById($id)
    {

        $db = Db::instance();
        $sql = 'select * from ' . static::TABLE .
            ' where id=:id';
        $task = $db->query($sql, [':id' => $id], static::class);

        if (empty($task)) {
            header("Location: /task/create");
            exit();

        }
        return $task[0];
    }

    public function insertTask()
    {

        if (!$this->isNew()) {
            return;
        }


        $column = [];
        $values = [];
        foreach ($this as $key => $v) {

            if ('id' === $key) {
                continue;
            }

            $column[] = $key;
            $values[':' . $key] = $v;
        }


        $sql = '
insert into ' . static::TABLE . '
(' . implode(',', $column) . ')
values (' . implode(',', array_keys($values)) . ')';

        $db = Db::instance();

        return $db->insert($sql, $values);
    }

    public function isNew(): bool
    {

        return empty($this->id);
    }

    public function update()
    {

        if ($this->isNew()) {
            return;
        }

        $values = [];
        foreach ($this as $key => $v) {
            $values[':' . $key] = $v;
        }


        $sql = 'UPDATE task SET name=:name, email=:email, text=:text,status=:status, edited_by_admin=:edited_by_admin  WHERE id =:id';


        $db = Db::instance();
        $db->execute($sql, $values);
        return $db->getLastId();
    }

    public function setSuccess($message = '')
    {
        $_SESSION['success'] = $message;

    }


}
