<?php


namespace splx;


trait Singleton
{
    protected static  $instance;

    /**
     * Singleton constructor.
     */
    private function __construct()
    {
    }

    public static function instance()
    {
        if (null === static::$instance) {

            static::$instance = new static();
        }
        return static::$instance;
}

    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

}
